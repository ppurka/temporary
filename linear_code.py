
    def covering_radius(self, algorithm = "auto"):
        r"""
        The covering radius of a linear code `C` is the smallest number `r`
        with the property that each element `v` of the ambient vector space
        of `C` has at most a distance `r` to the code `C`. So for each
        vector `v` there must be an element `c` of `C` with `d(v,c) \leq  r`.
        A binary linear code with reasonable small covering radius is often
        referred to as a covering code.

        For example, if `C` is a perfect code, the covering radius is equal
        to `t`, the number of errors the code can correct, where `d = 2t+1`,
        with `d` the minimum distance of `C`.

        If ``algorithm = "guava"`` use optional gap_packages (Guava package)
        Wraps Guava's ``CoveringRadius`` command.
        If ``algorithm = "auto"`` checks if gap_packages is installed uses ``"guava"``
        otherwise uses ``"coset_leader"`` algorithm (only for binary codes)

        INPUT:

        - ``algorithm`` -- Method to be used, one of "coset_leader","auto" or "guava"

        OUTPUT:

        - Integer representing covering radius

        EXAMPLES::

            sage: G=matrix(GF(2),[[1,0,0,1,1,1],[0,1,0,0,1,1],[0,0,1,1,0,1]])
            sage: C = LinearCode(G)
            sage: C.covering_radius("auto")
            2
            sage: C.covering_radius("coset_leader")
            2
            sage: C.covering_radius("guava")  # optional - gap_package(Guava package)
            2

            sage: C = HammingCode(5,GF(2))
            sage: C.covering_radius("auto")
            1
            sage: C.covering_radius("coset_leader")
            1
            sage: C.covering_radius("guava")  # optional - gap_packages (Guava package)
            1

        It is an error to... ::

            sage: C = BinaryGolayCode()
            sage: C.covering_radius("singular")
            Traceback (most recent call last):
            ...
            ValueError: The algorithm argument must be one of 'coset_leader','auto' or 'guava'; got 'singular'
        """

        if algorithm not in ("guava","coset_leader","auto"):
            raise ValueError("The algorithm argument must be one of 'coset_leader','auto' or 'guava'; got '{0}'".format(algorithm))

        from sage.misc.package import is_package_installed
        if algorithm == "auto":
            if is_package_installed("gap_packages"):
                algorithm = "guava"
            else:
                algorithm = "coset_leader"

        if algorithm == "guava":
            F = self.base_ring()
            G = self.gen_mat()
            gapG = gap(G)
            C = gapG.GeneratorMatCode(gap(F))
            r = C.CoveringRadius()
            try:
                return ZZ(r)
            except TypeError:
                raise RuntimeError("the covering radius of this code cannot be computed by Guava")
        elif algorithm == "coset_leader":
            if self.base_ring().order() != 2:
                raise NotImplementedError("The algorithm 'coset_leader' is only implemented for binary codes")
            CL = self.coset_leaders()
            return CL[-1][0].hamming_weight()

    def _insert_nextnew(self,v,List,order):
        r"""
        Inserts vectors ``v + e_i``, with ``i`` not in support of ``v`` and ``e_i`` standard basis vector,
        in List according the order ``order``.
        function for internal use only

        EXAMPLE::

            sage: C = HammingCode(3,GF(2))
            sage: v = vector(GF(2),C.length())
            sage: List = []
            sage: t = TermOrder("degrevlex")
            sage: C._insert_nextnew(v,List,t)
            [(1, 0, 0, 0, 0, 0, 0),
             (0, 1, 0, 0, 0, 0, 0),
             (0, 0, 1, 0, 0, 0, 0),
             (0, 0, 0, 1, 0, 0, 0),
             (0, 0, 0, 0, 1, 0, 0),
             (0, 0, 0, 0, 0, 1, 0),
             (0, 0, 0, 0, 0, 0, 1)]
        """
        s = [i for i in range(self.length()) if i not in v.support()]
        from sage.rings.polynomial.polydict import ETuple
        from sage.matrix.constructor import identity_matrix
        B = identity_matrix(GF(2),self.length())
        comp = getattr(order,'compare_tuples_'+order.name())
        if not List:
            List.append(ETuple((v+B[s[0]]).list()))
        for i in s:
            new = ETuple((v+B[i]).list())
            if comp(List[-1],new)==1:
                List.append(new)
                continue
            j = 0
            while comp(List[j],new)==1:
                j+=1
            if new != List[j]:
                List.insert(j,new)
        return List

    def coset_leaders(self, order = "degrevlex"):
        r"""
        Return the set of coset leaders of ``self``
        which are the set of vectors with minimum weight for
        each coset.

        INPUT:

        - ``order`` --string (default-"degrevlex") -- a degree ordering
        See: mod:`~sage.rings.polynomial.term_order` for the orderings.

        OUTPUT:

        - List which contains a list of coset leaders for each coset

        EXAMPLES::

            sage: C = HammingCode(3,GF(2))
            sage: C.coset_leaders()
            [[(0, 0, 0, 0, 0, 0, 0)],
             [(0, 0, 0, 0, 0, 0, 1)],
             [(0, 0, 0, 0, 0, 1, 0)],
             [(0, 0, 0, 0, 1, 0, 0)],
             [(0, 0, 0, 1, 0, 0, 0)],
             [(0, 0, 1, 0, 0, 0, 0)],
             [(0, 1, 0, 0, 0, 0, 0)],
             [(1, 0, 0, 0, 0, 0, 0)]]

             sage: G=matrix(GF(2),[[1,0,0,1,1,1],[0,1,0,0,1,1],[0,0,1,1,0,1]])
             sage: C= LinearCode(G)
             sage: C.coset_leaders()
             [[(0, 0, 0, 0, 0, 0)],
              [(0, 0, 0, 0, 0, 1)],
              [(0, 0, 0, 0, 1, 0)],
              [(0, 0, 0, 1, 0, 0)],
              [(0, 0, 1, 0, 0, 0)],
              [(0, 1, 0, 0, 0, 0)],
              [(1, 0, 0, 0, 0, 0)],
              [(1, 0, 0, 0, 0, 1), (0, 0, 0, 1, 1, 0), (0, 1, 1, 0, 0, 0)]]
        """
        if not self.base_ring().order() == 2:
            raise NoImplementedError("The 'coset_leaders' function is only implemented for binary codes")
        if not self.__coset_leaders:
            from sage.rings.polynomial.polydict import ETuple
            from sage.rings.polynomial.term_order import TermOrder
            t_order = TermOrder(order)
            H = self.check_mat().transpose()
            CL = []
            N = []
            S = []
            List = [ETuple(vector(GF(2),self.length()).list())]
            while List:
                w = vector(GF(2),List.pop())
                s = w*H
                if s in S:
                    j = S.index(s)
                    if w.hamming_weight()==N[j].hamming_weight():
                        CL[j].append(w)
                        List = self._insert_nextnew(w,List,t_order)
                else:
                    N.append(w)
                    S.append(s)
                    CL.append([w])
                    List = self._insert_nextnew(w,List,t_order)
            self.__coset_leaders = CL
        return self.__coset_leaders

    def newton_radius(self):
        r"""
        The newton radius of a binary code is the largest weight of any vector that can
        be uniquely corrected, or equivalently, it is the largest value among the cosets
        with only one coset leader since an error is uniquely correctable if and only if
        it is the unique coset leader in its coset.

        OUTPUT:

        - Positive integer indicating the newton radius

        EXAMPLE::

            sage: H = matrix(GF(2),[[1,0,0,0,1,0,0,0,0,0],[1,0,1,1,0,1,0,0,0,0],[1,1,0,1,0,0,1,0,0,0],[1,1,1,0,0,0,0,1,0,0],[1,1,1,1,0,0,0,0,1,0],[1,1,1,1,0,0,0,0,0,1]])
            sage: C = LinearCodeFromCheckMatrix(H)
            sage: C.newton_radius()
            3

            sage: C = HammingCode(4,GF(2))
            sage: C.newton_radius()
            1
        """
        if not self.base_ring().order() == 2:
            raise NotImplementedError("The 'newton_radius' function is only implemented for binary codes")
        CL = self.coset_leaders()
        for v in CL[::-1]:
            if len(v)==1:
                return v[0].hamming_weight()

    def weight_distribution_coset(self):
        r"""
        The Weight Distribution of the Coset Leaders of a binary code `C` is the list
        in which the entry ``i`` has the number of cosets with weight ``i``.

        OUTPUT:

        - Tuple of length the code's length in which every entry is a integer

        EXAMPLE::

            sage: G=matrix(GF(2),[[1,0,0,1,1,1],[0,1,0,0,1,1],[0,0,1,1,0,1]])
            sage: C= LinearCode(G)
            sage: C.weight_distribution_coset()
            (1, 6, 1, 0, 0, 0)

            sage: C = HammingCode(4,GF(2))
            sage: C.weight_distribution_coset()
            (1, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

            sage: H = matrix(GF(2),[[1,0,0,0,1,0,0,0,0,0],\
                                    [1,0,1,1,0,1,0,0,0,0],\
                                    [1,1,0,1,0,0,1,0,0,0],\
                                    [1,1,1,0,0,0,0,1,0,0],\
                                    [1,1,1,1,0,0,0,0,1,0],\
                                    [1,1,1,1,0,0,0,0,0,1]])
            sage: C = LinearCodeFromCheckMatrix(H)
            sage: C.weight_distribution_coset()
            (1, 10, 30, 23, 0, 0, 0, 0, 0, 0)


        """
        if not self.base_ring().order() == 2:
            raise NotImplementedError("The 'weight_distribution_coset' function is only implemented for binary codes")
        CL = self.coset_leaders()
        w_d = [0]*self.length()
        for c in CL:
            w_d[c[0].hamming_weight()]+=1
        return tuple(w_d)

    def grobner_representation(self, order ="degrevlex"):
        r"""
        Returns a Grobner representation of the code
        A Grobner representation of an `[n,k]` binary linear code
        `C` is a pair `(N,\Phi)` such that:

        #. `N` is a transversal of the cosets in `F_2^n/C`
        (i.e one element of each coset) verifying that `0` belongs to `N` and for
        each `n` in N`\`0` there exists `e_i`
        with `i` in `\{1,...,n\}` such that `n = n' + e_i` with `n'` in  `N`
        #. `\phi` is a function called ``Matphi function`` that maps each pair `(n,e_i)`
        with `n` in `N` to the element of `N` representing the coset that contains `n+e_i`.


        INPUT:

        - ``order`` --string (default-"degrevlex") -- a degree ordering
        See: mod:`~sage.rings.polynomial.term_order` for the orderings.

        OUTPUT:

        - List with one vector of each coset of the code ``self`` `(N)`

        - Dictionary representing the ``Matphi function``  `(\Phi)`

        EXAMPLE::

        sage: H = matrix(GF(2),[[1,0,0,1,1,1],[0,1,0,1,0,1],[0,0,1,0,1,1]])
        sage: C = LinearCodeFromCheckMatrix(H)
        sage: GR = C.grobner_representation()
        sage: dic = GR[1]
        sage: dic[((0,1,0,0,0,0),1)]
        (0, 0, 0, 0, 0, 0)
        sage: dic[((0,1,0,0,0,0),2)]
        (1, 0, 0, 0, 0, 1)
        """
        if not self.base_ring().order() == 2:
            raise NotImplementedError("The 'grobner_representation' function is only implemented for binary codes")
        if not self.__grobner_representation:
            from sage.rings.polynomial.polydict import ETuple
            from sage.rings.polynomial.term_order import TermOrder
            t_order = TermOrder(order)
            H = self.check_mat().transpose()
            B = self.ambient_space().basis()
            S = []
            N = []
            List = [ETuple(vector(GF(2),self.length()).list())]
            PHI = {}
            while List:
                w = vector(GF(2),List.pop())
                s = w*H
                j = 0
                if s in S:
                    j = S.index(s)
                if j:
                    for k in w.support():
                        if (w-B[k]) in N:
                            PHI[tuple([tuple(w-B[k]),k])] = tuple(N[j])
                else:
                    N.append(w)
                    S.append(s)
                    List = self._insert_nextnew(w,List,t_order)
                    for k in w.support():
                        if (w-B[k]) in N:
                            PHI[tuple([tuple(w-B[k]),k])] = tuple(w)
                            PHI[tuple([tuple(w),k])] = tuple(w-B[k])
            self.__grobner_representation = [N,PHI]
        return self.__grobner_representation

    def decode_gdda(self,y):
        r"""
        Gradient descent decoding algorithm: decodes the received word ``y`` to an element
        ``c`` in this code using its grobner representation.

        INPUT:

        - ``y`` --Vector of the same length as a codeword

        OUTPUT:

        - Vector representing a word in this code closest to ``y``

        EXAMPLES::

            sage: C = HammingCode(4,GF(2))
            sage: v = vector(GF(2),(0,1,1,1,0,1,1,1,0,1,0,1,0,1,0))
            sage: C.decode_gdda(v)
            (0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0)

            sage: G = matrix(GF(2),[[1,0,0,1,1,1],[0,1,0,0,1,1],[0,0,1,1,0,1]])
            sage: C = LinearCode(G)
            sage: v = vector(GF(2),(1,1,1,1,1,0))
            sage: C.decode_gdda(v)
            (0, 1, 1, 1, 1, 0)
            sage: v = vector(GF(2),(1,0,1,0,1,1))
            sage: C.decode_gdda(v)
            (1, 0, 1, 0, 1, 0)

        """
        if not self.base_ring().order() == 2:
            raise NotImplementedError("The 'GDDA' algorithm is only implemented for binary codes")
        GR_dic = self.grobner_representation()[1]
        s = y.support()
        y_t = (0,)*self.length()
        for i in range (self.length()):
            if i in s:
                #print y_t
                y_t = GR_dic[(y_t,i)]
        return vector(GF(2),y_t)+y

    def _multiple(self,w,grob):
        r"""
        This fucntion checks if monomial ``w`` is multiple of any leader term of elements
        in the grobner representation ``grob``
        function for internal use only.
        """
        for g in grob:
            if set(g[0]).issubset(set(w.nonzero_positions())):
                return True
        return False

    def grobner_basis(self, order = "degrevlex"):
        r"""
        Computes the grobner basis of the ideal associated to linear code ``self`` w.r.t
        monomial ordering ``order``.

        INPUT:

        - ``order`` --string (default-"degrevlex") -- a degree ordering
        See: mod:`~sage.rings.polynomial.term_order` for the orderings.

        OUTPUT:

        - List which elements are lists representing, each one, a element of
        the grobner basis. In this way:
        element `[[4,5],[0,2]]` represent binomial `x4*x5 + x0*x2`
        Last element of the list(have only 0's or 2's) represent the varialbles such that
        the square of that variable belongs to the grobner representation:
        [0,2,0,0,2] indicates that `x1^2+1` and `x4^2+1` are elements of the grobner basis.

        EXAMPLES::
        """
        from sage.rings.polynomial.term_order import TermOrder
        from sage.rings.polynomial.polydict import ETuple
        from sage.combinat.permutation import Permutations
        t = TermOrder(order)
        n = self.length()
        maxdegree = n - self.dimension()+2
        genMat = Matrix(GF(2),self.dimension()+1,n)
        genMat.set_block(1,0,self.gen_mat())
        grob_b = []
        w2 = {}
        for g in genMat:
            w2[tuple(g)]=vector(GF(2),[0]*n)
        comp = getattr(t,'compare_tuples_'+t.name())
        for i in range(1,maxdegree):
            gen_per = [0]*(n-i) + [1]*i
            w1 = Permutations(gen_per).list()
            w1 = [ETuple(w) for w in w1 if not self._multiple(ETuple(w),grob_b)]
            if not w1:
                break
            w1 = sorted(w1,cmp = comp,reverse = True)
            while w1:
                v = vector(GF(2),w1.pop())
                for g in genMat:
                    temp = tuple(v + g)
                    if temp in w2:
                        grob_b.append([v.nonzero_positions(),w2[temp].nonzero_positions()])
                        break
                    w2[temp] = v
        #adding squares
        x = [2]*self.length()
        for g in grob_b:
            if len(g[0]) == 1:
                x[g[0][0]] = 0
            else:
                break
        grob_b.append(x)
        return grob_b

    #binary case
    def groebner_basis(self, order = "degrevlex"):
        r"""
        Computes the grobner basis of the ideal associated to linear code ``self`` w.r.t
        monomial ordering ``order``. Using algorithm fglm from singular.

        INPUT:

        - ``order`` --string (default-"degrevlex") -- a degree ordering
        See: mod:`~sage.rings.polynomial.term_order` for the orderings.

        OUTPUT:

        - Sequence of polynomials representing a reduced groebner basis.

        EXAMPLES::

            sage: C = WalshCode(2)
            sage: C.groebner_basis()
            [x0^2 + 1, x3^2 + 1, x1 + x3, x2 + x3]

            sage: C = HammingCode(3,GF(2))
            sage: C.groebner_basis()
            [x0^2 + 1,
             x0*x1 + x2,
             x1^2 + 1,
             x0*x2 + x1,
             x1*x2 + x0,
             x2^2 + 1,
             x0*x3 + x4,
             x1*x3 + x5,
             x2*x3 + x6,
             x3^2 + 1,
             x0*x4 + x3,
             x1*x4 + x6,
             x2*x4 + x5,
             x3*x4 + x0,
             x4^2 + 1,
             x0*x5 + x6,
             x1*x5 + x3,
             x2*x5 + x4,
             x3*x5 + x1,
             x4*x5 + x2,
             x5^2 + 1,
             x0*x6 + x5,
             x1*x6 + x4,
             x2*x6 + x3,
             x3*x6 + x2,
             x4*x6 + x1,
             x5*x6 + x0,
             x6^2 + 1]

            sage: G = Matrix(GF(2),[[1,0,1,1],[0,1,1,0]])
            sage: C = LinearCode(G)
            sage: C.groebner_basis()
            [x0^2 + 1, x0*x2 + x3, x2^2 + 1, x0*x3 + x2, x2*x3 + x0, x3^2 + 1, x1 + x2]
        """
        if not self.base_ring().order() == 2:
            raise NotImplementedError("The groebner basis method for a code is only implemented for binary codes")
        R = PolynomialRing(GF(2),self.length(),'x',order = order)
        Rgens = R.gens()
        gens = []
        for g in self.gen_mat():
            p = prod(Rgens[i] for i in g.support())
            gens.append(p-1)
        I = R.ideal([_**2 -1 for _ in R.gens()]+ gens)
        return list(I.groebner_basis('libsingular:stdfglm'))

    def _test_set(self):
        r"""
        Let `G = \{g_1,...,g_s\}` be a reduced Grobner basis of the ideal
        associated to the code ``self``. Where `g_i = X^{g_i^+} - X^{g_i^-}`
        A test-set for the code ``self`` is defined as:
        `T = \{ g_i^+ - g_i^- \vert i = 1,...,s\}`
        This function returns the test-set for ``self``.(For internal use only)

        .. Note::
            The test set doesn't contain duplicates, so cardinality of test-set is less equal
            than grobner basis cardinality.

        OUTPUT:

        - List of vectors representing the test-set.

        EXAMPLE::

            sage: C = HammingCode(3,GF(2))
            sage: len(C.groebner_basis())
            28
            sage: C._test_set()
            [(0, 0, 0, 0, 0, 0, 0),
             (1, 1, 1, 0, 0, 0, 0),
             (1, 0, 0, 1, 1, 0, 0),
             (0, 1, 0, 1, 0, 1, 0),
             (0, 0, 1, 1, 0, 0, 1),
             (0, 1, 0, 0, 1, 0, 1),
             (0, 0, 1, 0, 1, 1, 0),
             (1, 0, 0, 0, 0, 1, 1)]
        """
        if not self.__groebner_basis_test_set:
            GB = self.groebner_basis()
            t_s = [vector(GF(2),gb.exponents()[0].eadd(gb.exponents()[1])) for gb in GB]
            for t in t_s:
                if t not in self.__groebner_basis_test_set:
                    self.__groebner_basis_test_set.append(t)
        return self.__groebner_basis_test_set

    def decode_gb(self, v):
        r"""
        Gradient descent decoding algorithm: decodes the received word ``y`` to an element
        ``c`` in this code using the grobner basis of the ideal associated to ``self``.

        INPUT:

        - ``y`` --Vector of the same length as a codeword

        OUTPUT:

        - Vector representing a word in this code closest to ``y``

        EXAMPLES::

            sage: C = WalshCode(3)
            sage: v = vector(GF(2),(0, 1, 0, 1, 1, 0, 0, 0))
            sage: dec_word = C.decode_gb(v)
            sage: dec_word
            (0, 1, 0, 1, 1, 0, 1, 0)
            sage: dec_word in C
            True

            sage: G = matrix(GF(2),[[1,0,0,0,1,1,1,1,1,1],[0,1,0,0,0,0,1,1,1,1],[0,0,1,0,0,1,0,1,1,1],[0,0,0,1,0,1,1,0,1,1]])
            sage: C = LinearCode(G)
            sage: v = vector(GF(2),(0, 1, 0, 1, 0, 1, 1, 0, 0, 1))
            sage: C.decode_gb(v)
            (0, 1, 0, 1, 0, 1, 0, 1, 0, 0)

            sage: C = HammingCode(4,GF(2))
            sage: v = vector(GF(2),(0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0))
            sage: C.decode_gb(v)
            (0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0)
        """
        if not self.base_ring().order() == 2:
            raise NotImplementedError("The 'gdda' algorithm using groebner basis is only implemented for binary codes")
        t_s = self._test_set()
        c = vector(GF(2),self.length())
        y = vector(GF(2),v)
        for t in t_s:
            if (y+t).hamming_weight() < y.hamming_weight():
                #print t
                c = c+t
                y = y+t
        return c

    def _multiple_fq(self,w,grob):
        r"""
        This function checks if a polynomial term represented(exponents) by `w` is
        multiple of any leader term of the Groebner basis `groebner_basis`.

        INPUT:

            - `w` -- Vector representing the exponents of a polynomial term
            - `groebner_basis` -- List representing the groebner basis elements so far.
               The variables we are working with are in the form ``x_{ij}``.
               So the first entry of each element in `groebner_basis` must be the support
               of a vector in which each entry indicate the index ``i`` of the variable
               that is present in the leader term of the groebner basis element.
               And the second entry are the values ``j`` for each entry ``i`` in the support.

        OUTPUT:

            - `True` if polynomial term represented by `w` is multiple of
                any leader term of the groebner representation. `False` otherwise.

        EXAMPLES::

        """
        ws = set(w.support())
        for g in grob:
            if g[0].issubset(ws):
                wsl = [w[i] for i in g[0]]
                if g[1]==wsl:
                    return True
        return False

    def old_grobner_fglm(self):
        from sage.combinat.permutation import Permutations
        from sage.combinat.combination import Combinations
        n = self.length()
        maxdegree = n - self.dimension()+2
        Fq = self.base_ring()
        v1 = vector(Fq,[0]*n)
        genMat = [v1]
        alpha = Fq.primitive_element()
        Fql = Fq.list()[1:]
        for g in self.gen_mat():
            genMat.append(g)
            genMat.append(alpha*g)
        #stores grobner basis
        grob_b = []
        #stores leader terms of grobner basis
        #in a convenient way to check for multiples
        grob_bb=[]
        w2 = {}
        for g in genMat:
            w2[tuple(g)]= v1
        vC = []
        for i in range(1,maxdegree):
            vC=vC+Fql
            #create all vector of weight i
            comb = Combinations(vC,i)
            w1 = []
            for gen_per in comb:
                w1 = w1+Permutations(gen_per+[0]*(n-i)).list()
            #eliminate multiples of leader terms of the grobner basis
            w1 = [vector(Fq,w) for w in w1 if not self._multiple_fq(vector(w),grob_bb)]
            if not w1:
                break
            w1.sort()
            while w1:
                v = w1.pop()
                for g in genMat:
                    temp = tuple(v + g)
                    if temp in w2:
                        grob_b.append([v,w2[temp]])
                        grob_bb.append([set(v.support()),v.list_from_positions(v.support())])
                        break
                    w2[temp] = v
        return grob_b

    #computes grobner basis of the ideal associated to the code.
    #with the get_vectors_fast() optimization
    def groebner_basis_fglm(self):
        """
        This function compute the grobner basis of the ideal associated to
        code `self`, using an adapted fglm algorithm for this case, and
        a graduated order implicitly.
        In this algorithm we use vectors of length's code dimension, the value
        entry ``x`` in position ``i`` represents the variable ``x_{ij}`` where
        ``j`` is given by ``x = \alpha^j``, ``\alpha`` is a generator of the
        finite field of `self`.

        OUTPUT:

            - Generator iterable object with grobner basis elements as vectors.

        EXAMPLES::

            sage: C = HammingCode(2,GF(3))
            sage: list(C.groebner_basis_fglm())
            [((0, 0, 2, 2), (1, 0, 0, 0)),
             ((0, 0, 1, 2), (0, 2, 0, 0)),
             ((0, 0, 2, 1), (0, 1, 0, 0)),
             ((0, 0, 1, 1), (2, 0, 0, 0)),
             ((0, 2, 0, 2), (2, 0, 0, 0)),
             ((0, 1, 0, 2), (0, 0, 2, 0)),
             ((0, 2, 0, 1), (0, 0, 1, 0)),
             ((0, 1, 0, 1), (1, 0, 0, 0)),
             ((0, 2, 2, 0), (0, 0, 0, 2)),
             ((0, 1, 2, 0), (0, 2, 0, 2)),
             ((0, 2, 1, 0), (0, 0, 2, 2)),
             ((0, 1, 1, 0), (0, 0, 0, 1)),
             ((2, 0, 0, 2), (0, 0, 1, 0)),
             ((1, 0, 0, 2), (0, 0, 2, 1)),
             ((2, 0, 0, 1), (0, 2, 0, 0)),
             ((1, 0, 0, 1), (0, 0, 2, 0)),
             ((2, 0, 2, 0), (0, 0, 0, 1)),
             ((1, 0, 2, 0), (2, 0, 0, 1)),
             ((2, 0, 1, 0), (0, 0, 2, 1)),
             ((1, 0, 1, 0), (0, 0, 0, 2)),
             ((1, 2, 0, 0), (0, 0, 0, 1)),
             ((2, 1, 0, 0), (0, 0, 0, 2)),
             ((1, 1, 0, 0), (0, 2, 0, 1))]
        """
        from sage.combinat.combination import Combinations
        from sage.combinat.cartesian_product import CartesianProduct
        n = self.length()
        maxdegree = n - self.dimension()+2
        Fq = self.base_ring()
        v1 = vector(Fq,n)
        genMat = [v1]
        alpha = Fq.primitive_element()
        Fqstar = Fq.list()[1:]
        One = Fq(1)
        Fqstar_wmax = [Fqstar]*(n-self.dimension())
        if Fq.is_prime_field():
            for g in self.gen_mat():
                genMat.append(g)
        else:
            Fqstar1 = Fqstar[:]
            Fqstar1.remove(One)
            for g in self.gen_mat():
                genMat.append(g)
                genMat.extend([a*g for a in Fqstar1])
        #stores leader terms of grobner basis
        #in a convenient way to check for multiples
        grob_bb=[]
        w2 = {}
        w1 = []
        for g in genMat:
            w2[tuple(g)]= v1
        for wt in xrange(1,maxdegree):
            for one_pos in xrange(n-wt+1):
                for c in Combinations(xrange(one_pos+1,n),wt-1):
                    v = v1.__copy__()
                    v[one_pos] = One
                    for values in CartesianProduct(*(Fqstar_wmax[:wt-1])):
                        for vi,ci in zip(values,c):
                            v[ci] = vi
                        if not self._multiple_fq(v,grob_bb):
                            for qe in Fqstar:
                                v[one_pos] = qe
                                w1.append(v.__copy__())
            if not w1:
                break
            while w1:
                v = w1.pop()
                for g in genMat:
                    temp = tuple(v + g)
                    if temp in w2:
                        grob_bb.append([set(v.support()),v.list_from_positions(v.support())])
                        yield (v,w2[temp])
                        break
                    w2[temp] = v


    #creat test set
    def _t_s(self):
        r"""
        Let `G = \{g_1,...,g_s\}` be a reduced Grobner basis of the ideal
        associated to the code ``self``. Where `g_i = X^{g_i^+} - X^{g_i^-}`
        A test-set for the code ``self`` is defined as:
        `T = \{ g_i^+ - g_i^- \vert i = 1,...,s\}`
        This function returns the test-set for ``self``.(For internal use only)

        OUTPUT:

        - List of vectors representing the test-set.

        EXAMPLES::

            sage: C = BCHCode(8,3,GF(3))
            sage: C._t_s()
            [(0, 0, 0, 0, 2, 0, 2, 2),
             (0, 0, 0, 0, 1, 0, 1, 1),
             (0, 0, 0, 2, 0, 2, 2, 0),
             (0, 0, 0, 1, 0, 1, 1, 0),
             (0, 0, 2, 0, 2, 2, 0, 0),
             (0, 0, 1, 0, 1, 1, 0, 0),
             (0, 0, 0, 1, 2, 1, 0, 2),
             (0, 0, 0, 2, 1, 2, 0, 1),
             (0, 2, 0, 2, 2, 0, 0, 0),
             (0, 1, 0, 1, 1, 0, 0, 0),
             (0, 0, 1, 0, 0, 1, 2, 2),
             (0, 0, 2, 0, 0, 2, 1, 1),
             (0, 0, 1, 2, 1, 0, 2, 0),
             (2, 0, 2, 2, 0, 0, 0, 0),
             (1, 0, 1, 1, 0, 0, 0, 0),
             (0, 1, 0, 1, 0, 0, 2, 2),
             (0, 1, 0, 0, 1, 2, 2, 0),
             (0, 2, 0, 0, 2, 1, 1, 0),
             (0, 1, 2, 1, 0, 2, 0, 0),
             (0, 2, 1, 2, 0, 1, 0, 0),
             (1, 0, 1, 0, 0, 2, 2, 0),
             (1, 0, 0, 1, 2, 2, 0, 0),
             (2, 0, 0, 2, 1, 1, 0, 0),
             (1, 2, 1, 0, 2, 0, 0, 0),
             (2, 1, 2, 0, 1, 0, 0, 0)]
        """
        if not self.__groebner_basis_test_set:
            GB = self.groebner_basis_fglm()
            t_s = [g[0]-g[1] for g in GB]
            for t in t_s:
                if t not in self.__groebner_basis_test_set:
                    self.__groebner_basis_test_set.append(t)
        return self.__groebner_basis_test_set

    #decoding algorithm over F_q
    def decode_fq(self,v):
        r"""
        Gradient descent decoding algorithm: decodes the received word ``y`` to an element
        ``c``, in this code, using the grobner basis of the ideal associated to ``self``.

        INPUT:

        - ``y`` --Vector of the same length as a codeword

        OUTPUT:

        - Vector representing a word in this code closest to ``y``

        EXAMPLES::

            sage: C = BCHCode(8,3,GF(3))
            sage: v = vector(GF(3),(2, 2, 1, 2, 1, 2, 0, 0))
            sage: C.decode_fq(v)
            (1, 2, 1, 2, 2, 2, 2, 0)

            sage: F = GF(4,'a')
            sage: a = F.primitive_element()
            sage: C = HammingCode(2,F)
            sage: v = vector(F,(a, 0, a + 1, a + 1, a + 1))
            sage: C.decode_fq(v)
            (a + 1, 0, a + 1, a + 1, a + 1)

            sage: C = ReedSolomonCode(6,4,GF(7))
            sage: v = vector(GF(7),(5, 5, 4, 3, 6, 2))
            sage: C.decode_fq(v)
            (0, 6, 3, 5, 5, 3)
        """
        t_s = self._t_s()
        c = vector(self.base_ring(),self.length())
        y = vector(self.base_ring(),v)
        for t in t_s:
            if (y-t).hamming_weight() < y.hamming_weight():
                c = c+t
                y = y+t
        return c

    #compute the error correcting capaticy of the code
    def correcting_capacity(self):
        r"""
        The error correcting capacity ``t`` of a code is the number of errors
        this code can correct. It's well known that ``t \leq \floor \lfloor d/2 \rfloor``.

        OUTPUT:

        - Integer representing the error correcting capacity.

        EXAMPLES::

            sage: C = ReedSolomonCode(7,3,GF(7))
            sage: C.correcting_capacity()
            2

            sage: C = HammingCode(2,GF(5))
            sage: C.minimum_distance()
            3
            sage: C.correcting_capacity()
            1
        """
        gb = self.groebner_basis_fglm().next()
        return gb[0].hamming_weight()-1

    #create the ideal associated with the code
    def create_ideal(self):
        Fq = self.base_ring()
        q = Fq.order()
        n = self.length()+1
        #naming variables. Not necessary, we can
        #eliminate it later, and speed up a little bit.
        x = 'x'
        Var = []
        for k in range(1,n):
            x_ = x+str(k)
            Var.extend([x_+str(i) for i in range(1,q)])
        ##############
        q-=1
        n-=1
        #creating the Ring with variables
        R = PolynomialRing(Fq,q*n,Var)
        X = R.gens()
        #separate X1,X2,...,Xn in q-1 components each
        Var = [X[i*q:(i+1)*q] for i in range(n)]
        M = Fq.addition_table().table()
        pol_I = []
        #create R_I ideal with addition table
        for i in range(q):
            for j in range(q):
                if M[i+1][j+1]==0:
                    pol_I.extend([Var[k][i]*Var[k][j]-1 for k in range(n)] )
                else:
                    m = M[i+1][j+1]-1
                    pol_I.extend([Var[k][i]*Var[k][j]-Var[k][m] for k in range(n)])
        a = Fq.primitive_element()
        d_pth={}
        p = 1
        #save log of each element of Fq in base 'a'
        for i in range(1,q+1):
            p*=a
            d_pth[p]=i
        G = self.gen_mat()
        Fq_l = Fq.list()[1:]
        #create
        for f in Fq_l:
            for g in G:
                w = f*g
                pol_I.append(prod([Var[i][d_pth[w[i]]-1] for i in w.support()])-1)
        return R.ideal(pol_I)

    def _insert_next_fq(self,v,List,Fqstar):
        s = [i for i in range(self.length()) if i not in v.support()]
        if List:
            for i in s:
                new = v.__copy__()
                for q in Fqstar:
                    new[i]=q
                    if new not in List:
                        List.append(new)
        else:
            for i in s:
                new = v.__copy__()
                for q in Fqstar:
                   new[i]=q
                   List.append(new.__copy__())

    def coset_leaders_fq(self):
        r"""
        Return the set of coset leaders of ``self``
        which are the set of vectors with minimum weight for
        each coset.

        OUTPUT:

        - List which contains a list of coset leaders for each coset

        EXAMPLES::

            sage: C = HammingCode(2,GF(5))
            sage: C.coset_leaders_fq()
            [[(0, 0, 0, 0, 0, 0)],
             [(1, 0, 0, 0, 0, 0)],
             [(2, 0, 0, 0, 0, 0)],
             [(3, 0, 0, 0, 0, 0)],
             [(4, 0, 0, 0, 0, 0)],
             [(0, 1, 0, 0, 0, 0)],
             [(0, 2, 0, 0, 0, 0)],
             [(0, 3, 0, 0, 0, 0)],
             [(0, 4, 0, 0, 0, 0)],
             [(0, 0, 1, 0, 0, 0)],
             [(0, 0, 2, 0, 0, 0)],
             [(0, 0, 3, 0, 0, 0)],
             [(0, 0, 4, 0, 0, 0)],
             [(0, 0, 0, 1, 0, 0)],
             [(0, 0, 0, 2, 0, 0)],
             [(0, 0, 0, 3, 0, 0)],
             [(0, 0, 0, 4, 0, 0)],
             [(0, 0, 0, 0, 1, 0)],
             [(0, 0, 0, 0, 2, 0)],
             [(0, 0, 0, 0, 3, 0)],
             [(0, 0, 0, 0, 4, 0)],
             [(0, 0, 0, 0, 0, 1)],
             [(0, 0, 0, 0, 0, 2)],
             [(0, 0, 0, 0, 0, 3)],
             [(0, 0, 0, 0, 0, 4)]]

            sage: G = matrix(GF(5),[[1,0,2],[3,2,4]])
            sage: C = LinearCode(G)
            sage: C.coset_leaders_fq()
            [[(0, 0, 0)],
             [(1, 0, 0), (0, 3, 0), (0, 0, 3)],
             [(2, 0, 0), (0, 1, 0), (0, 0, 1)],
             [(3, 0, 0), (0, 4, 0), (0, 0, 4)],
             [(4, 0, 0), (0, 2, 0), (0, 0, 2)]]
        """
        if not self.__coset_leaders:
            H = self.check_mat().transpose()
            List = [vector(self.base_ring(),self.length())]
            Fqstar = self.base_ring().list()[1:]
            CL = []
            N = []
            S = []
            while List:
                w = List.pop(0)
                s = w*H
                if s in S:
                    j = S.index(s)
                    if w.hamming_weight() == N[j].hamming_weight():
                        CL[j].append(w)
                        self._insert_next_fq(w,List,Fqstar)
                else:
                    N.append(w)
                    S.append(s)
                    CL.append([w])
                    self._insert_next_fq(w,List,Fqstar)
            self.__coset_leaders = CL
        return self.__coset_leaders

    def minimal_support(self):
        from sage.combinat.combination import Combinations
        from sage.combinat.cartesian_product import CartesianProduct
        n = self.length()*2
        maxdegree = 2*n - self.dimension()+2
        Fq = self.base_ring()
        v1 = vector(Fq,n)
        genMat = [v1]
        alpha = Fq.primitive_element()
        Fqstar = Fq.list()[1:]
        One = Fq(1)
        Fqstar_wmax = [Fqstar]*(n-self.dimension())
        Fqstar1 = Fqstar[:]
        Fqstar1.remove(One)
        for g in self.gen_mat():
            genMat.append(vector(Fq,g.list()+(-1*g).list()))
            genMat.extend([vector(Fq,(a*g).list()+(-1*a*g).list()) for a in Fqstar1])
        #stores leader terms of grobner basis
        #in a convenient way to check for multiples
        grob_bb=[]
        M=[]
        w2 = {}
        for g in genMat:
            w2[tuple(g)]= v1
        vC = []
        for wt in xrange(1,maxdegree):
            w1 = []
            for one_pos in xrange(n-wt+1):
                for c in Combinations(xrange(one_pos+1,n),wt-1):
                    v = v1.__copy__()
                    v[one_pos] = One
                    for values in CartesianProduct(*(Fqstar_wmax[:wt-1])):
                        for vi,ci in zip(values,c):
                            v[ci] = vi
                        if not self._multiple_fq(v,grob_bb):
                            for qe in Fqstar:
                                v[one_pos] = qe
                                w1.append(v.__copy__())
            if not w1:
                break
            while w1:
                v = w1.pop()
                for g in genMat:
                    temp = tuple(v + g)
                    if temp in w2:
                        grob_bb.append([set(v.support()),v.list_from_positions(v.support())])
                        m = v-w2[temp]
                        msupp = set(m.support())
                        if not any(x[1].issubset(msupp) for x in M):
                            M.append([m,msupp])
                        break
                    w2[temp] = v
        length = self.length()
        minwords = []
        for x in M:
            v = vector(Fq,x[0].list()[:length])
            for a in Fqstar:
                new = a*v
                if new not in minwords:
                    minwords.append(new)
        return minwords

